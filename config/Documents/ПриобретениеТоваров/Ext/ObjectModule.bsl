﻿
Процедура ОбработкаПроведения(Отказ, Режим)

	// регистр ОстаткиТоваров движения приход 
	Движения.ОстаткиТоваров.Записывать = Истина;
	Для Каждого ТекСтрокаТовары Из Товары Цикл
		Движение = Движения.ОстаткиТоваров.Добавить();
		Движение.ВидДвижения = ВидДвиженияНакопления.Приход;
		Движение.Период = Дата;
		Движение.Товар = ТекСтрокаТовары.Номенклатура;
		Движение.Количество = ТекСтрокаТовары.Количество;
	КонецЦикла;

КонецПроцедуры

//++Разработчик2 09.08.21
Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	Для каждого строка из ЭтотОбъект.Товары цикл
		Если не ЗначениеЗаполнено(строка.Номенклатура) тогда
			Отказ = истина;
			СообщениеПользователю = новый СообщениеПользователю;
			Индекс = ЭтотОбъект.Товары.индекс(строка);
			СообщениеПользователю.Текст = "Не заполнен товар";
			СообщениеПользователю.Поле = "ЭтотОбъект.Товары["+Индекс+"].Номенклатура";    
			СообщениеПользователю.УстановитьДанные(ЭтотОбъект);
			СообщениеПользователю.Сообщить(); 
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры  
//--Разработчик2
